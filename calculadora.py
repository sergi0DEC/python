# Desarrolladores: Carmen Ramirez, Neftaly Rocha, Sergio Escamilla

def suma(a, b):
    return a + b

def resta(a, b):
    return a - b

def multiplicacion(a, b):
    return a * b

def division(a, b):
    return a / b

# Función principal del programa
def calculadora():
    print("Bienvenido a la calculadora sencilla")

    while True:
        print("Opciones:")
        print("1. Suma")
        print("2. Resta")
        print("3. Multiplicación")
        print("4. División")
        print("5. Salir")

        opcion = input("Seleccione una opción: ")

        if opcion == '5':
            print("¡Hasta luego!")
            break

        num1 = float(input("Ingrese el primer número: "))
        num2 = float(input("Ingrese el segundo número: "))

        if opcion == '1':
            resultado = suma(num1, num2)
            print("Resultado:", resultado)
        elif opcion == '2':
            resultado = resta(num1, num2)
            print("Resultado:", resultado)
        elif opcion == '3':
            resultado = multiplicacion(num1, num2)
            print("Resultado:", resultado)
        elif opcion == '4':
            if num2 != 0:
                resultado = division(num1, num2)
                print("Resultado:", resultado)
            else:
                print("¡Error! No se puede dividir entre cero.")
        else:
            print("Opción inválida. Por favor, seleccione una opción válida.")

# Llamada a la función principal de la calculadora
calculadora()
